//
//  ViewController.m
//  ScrollView
//
//  Created by Stuart Adams on 1/13/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //initializing commonly used device attributes to varibles for quick access
    int screenWidth = self.view.frame.size.width;
    int screenHeight = self.view.frame.size.height;
    
    UIScrollView* scrlView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [scrlView setContentSize:CGSizeMake(screenWidth * 3, screenHeight * 3)];
    scrlView.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:scrlView];
    
    int count = 1; //using for page number increments and color switch
    
    //nested for loop to create color grid
    for (int i = 1; i <= 3; i++) {
        for (int j = 1; j <= 3; j++) {
            UIView* view = [[UIView alloc] initWithFrame:CGRectMake(screenWidth *(j - 1), screenHeight * (i-1), screenWidth, screenHeight)];
            
            //using a switch to determine page color
            switch (count) {
                case 1:
                    view.backgroundColor = [UIColor redColor];
                    break;
                case 2:
                    view.backgroundColor = [UIColor greenColor];
                    break;
                case 3:
                    view.backgroundColor = [UIColor blueColor];
                    break;
                case 4:
                    view.backgroundColor = [UIColor cyanColor];
                    break;
                case 5:
                    view.backgroundColor = [UIColor yellowColor];
                    break;
                case 6:
                    view.backgroundColor = [UIColor magentaColor];
                    break;
                case 7:
                    view.backgroundColor = [UIColor orangeColor];
                    break;
                case 8:
                    view.backgroundColor = [UIColor purpleColor];
                    break;
                case 9:
                    view.backgroundColor = [UIColor brownColor];
                    break;
                    
                default:
                    break;
            }
            
            //Creating page lable
            UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake((screenWidth * .5) - 50, (screenHeight * .5) - 50, 100, 100)];
            lbl.textColor = [UIColor blackColor];
            
            //creating a string with placeholders to allow for variable use within the string to autoincrement page numbers
            NSString* lblText = [NSString stringWithFormat:@"Page: %d", count++];
            lbl.text = lblText;
            
            //adding lbl to view
            [view addSubview:lbl];
            
            //enabling paging and andding view to scrlView
            [scrlView setPagingEnabled:true];
            [scrlView addSubview:view];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
